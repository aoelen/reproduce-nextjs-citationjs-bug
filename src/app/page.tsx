'use client';

import styles from './page.module.css';
// @ts-expect-error
import { Cite } from '@citation-js/core';
import '@citation-js/plugin-bibtex';
import '@citation-js/plugin-csl';
import '@citation-js/plugin-doi';
import { useEffect, useState } from 'react';

export default function Home() {
    const [bibliography, setBibliography] = useState('');
    useEffect(() => {
        const getCitation = async () => {
            const parsedCitation =
                await Cite.async(`@article{haras2018thermoelectricity,
        title={Thermoelectricity for IoT--A review},
        author={Haras, Maciej and Skotnicki, Thomas},
        journal={Nano Energy},
        volume={54},
        pages={461--476},
        year={2018},
        publisher={Elsevier}
      }`);
            const _bibliography = parsedCitation.format('bibliography', {
                format: 'html',
                template: 'apa',
                lang: 'en-US',
            });
            setBibliography(_bibliography);
        };
        getCitation();
    });
    return (
        <div className={styles.page}>
            <div style={{ padding: 30, whiteSpace: 'pre' }}>{bibliography}</div>
        </div>
    );
}
